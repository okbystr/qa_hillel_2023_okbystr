str1 = "open 3 files one two 3 stop"
counter = 0
for word_item in str1.split():
    if word_item.isalpha():
        counter += 1
        if counter == 3:
            print(True)
            break
    else:
        counter = 0
else:
    print(False)
print('ok')

str2 = "open 3 files one 2 3 stop"
counter = 0
for word_item in str2.split():
    if word_item.isalpha():
        counter += 1
        if counter == 3:
            print(True)
            break
    else:
        counter = 0
else:
    print(False)
print('ok')

str3 = "open 3 files 1 2 3 stop"
counter = 0
for word_item in str3.split():
    if word_item.isalpha():
        counter += 1
        if counter == 3:
            print(True)
            break
    else:
        counter = 0
else:
    print(False)
print('ok')

str4 = " "
counter = 0
for word_item in str4.split():
    if word_item.isalpha():
        counter += 1
        if counter == 3:
            print(True)
            break
    else:
        counter = 0
else:
    print(False)
print('ok')

str5 = "open three files one 2 3 stop"
counter = 0
for word_item in str5.split():
    if word_item.isalpha():
        counter += 1
        if counter == 3:
            print(True)
            break
    else:
        counter = 0
else:
    print(False)
print('ok')