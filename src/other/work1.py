def sorted_divisors(x):
    divisors = []
    for n in range(1, x + 1):
        if x % n == 0:
            divisors.append(n)
    return sorted(divisors)
x = 150
result = sorted_divisors(x)
print(result)