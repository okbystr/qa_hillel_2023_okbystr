class TrainCar:
    def __init__(self, car_number, max_passengers=10):
        self.car_number = car_number
        self.max_passengers = max_passengers
        self.passengers = []
    def add_passenger(self, passenger_name, destination, place):
        if len(self.passengers) < self.max_passengers:
            passenger_info = {
                "passenger_name": passenger_name,
                "destination": destination,
                "place": place
            }
            self.passengers.append(passenger_info)

    def __len__(self):
        return len(self.passengers)
    def __str__(self):
        car_info = f'"traincart" :"{self.car_number}"\n'
        for idx, passenger in enumerate(self.passengers, start=1):
            car_info += f'"passenger_name": "{passenger["passenger_name"]}",\n'
            car_info += f'"destination": "{passenger["destination"]}",\n'
            car_info += f'"place": {passenger["place"]}\n'
            if idx < len(self.passengers):
                car_info += '\n'
        return car_info


class Train:
    def __init__(self):
        self.train_cars = []
    def add_train_car(self, train_car):
        self.train_cars.append(train_car)
    def __len__(self):
        return len(self.train_cars)
    def __str__(self):
        train_info = f'Train with {len(self.train_cars)} cars (excluding locomotive)\n'
        for idx, car in enumerate(self.train_cars, start=1):
            train_info += f'Car {idx}:\n{str(car)}\n'
            if idx < len(self.train_cars):
                train_info += '\n'
        return train_info


car1 = TrainCar(car_number=1)

car1.add_passenger("John Dow", "Station A", 1)
car1.add_passenger("Alex Dowson", "Station B", 2)

train = Train()

train.add_train_car(car1)

print(train)