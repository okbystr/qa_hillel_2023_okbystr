from datetime import datetime, timedelta
def modify_date(original_date, days, add=True):
    sign = 1 if add else -1
    delta = timedelta(days=sign * days)
    modified_date = original_date + delta
    return modified_date

given_date = datetime(2023, 11, 30, 17, 10, 20)
modified_date_after_addition = modify_date(given_date, 5, add=True)
modified_date_after_subtraction = modify_date(given_date, 10, add=False)

print("Задана дата:", given_date)
print("Дата після додавання 5 днів:", modified_date_after_addition)
print("Дата після віднімання 10 днів:", modified_date_after_subtraction)



from datetime import datetime

def calculate_age(birthdate):

    current_datetime = datetime.now()

    age_timedelta = current_datetime - birthdate
    age_timestamp = age_timedelta.total_seconds()
    birthdate_str = birthdate.strftime("%y-%m-%d %I:%M:%S %p")
    return age_timedelta, age_timestamp, birthdate_str

birthdate = datetime(2019, 5, 11, 12, 30, 00)
age_timedelta, age_timestamp, birthdate_str = calculate_age(birthdate)

print("Прожите часу:", age_timedelta)
print("Timestamp прожитого життя:", age_timestamp)
print("Час народження:", birthdate_str)